## Install

```shell
# Sets up the spring Git submodule
git submodule init
git submodule update

winget install -i LLVM.LLVM # Make sure to add to PATH
```

In the Visual Studio Installer (added to your system as a part of the Rust
SDK install) modify "Visual Studio Community" and add "Desktop Development with
C++".

## Update Spring Engine

```shell
# Removes the old spring submodule
git rm --cached spring
rm -rf spring

# Adds the new spring submodule
git submodule add https://github.com/beyond-all-reason/spring.git
```
